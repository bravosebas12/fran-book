import firebase from "firebase";


const firebaseConfig = {
    apiKey: "AIzaSyAcQpyq9xBBwqXh4eddiw7YntIxY9HY040",
    authDomain: "book-fran.firebaseapp.com",
    projectId: "book-fran",
    storageBucket: "book-fran.appspot.com",
    messagingSenderId: "85688944533",
    appId: "1:85688944533:web:f7f0251839635a8206d89c",
    measurementId: "G-MCJ3ZH7QVV"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebase.firestore();
  const auth  = firebase.auth();
  export {db,auth};