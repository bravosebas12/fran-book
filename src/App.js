import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Inicio from "./components/Inicio";
import Principal from "./components/Principal";
import Login from "./components/Login";


function App() {
  return (
    <Router>
      <div className="App">
        <Navbar expand="lg" bg="danger" variant="dark">
          <Navbar.Brand href="#home">Book-Fran</Navbar.Brand>
          <Nav className="ml-auto">
            <Nav.Link href="#nos">Nosotros</Nav.Link>
            <Nav.Link href="#car">Carrito</Nav.Link>
            <Nav.Link href="/login">Login</Nav.Link>
          </Nav>
        </Navbar>
        <Switch>
          <Route path="/" exact>
            <Inicio />
          </Route>

          <Route path="/principal">
            <Principal />
          </Route>

          <Route path="/login">
            <Login />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
